public class Circle {
    double radius = 1.0 ;

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public Circle() {
        System.out.println("ko có tham số");
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getArea(double radius){
        return radius * radius * 3.14;
    }
    public double getCircumference(double radius){
        return radius * 2 * 3.14 ;
    }
    @Override
    public String toString(){
        return "diện tích hình tròn : " + getArea(radius) + " , chu vi hình tròn : " + getCircumference(radius);
    }
}
